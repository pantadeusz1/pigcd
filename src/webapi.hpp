#ifndef ___WEBAPI__HPP_____
#define ___WEBAPI__HPP_____
/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <any>
#include <functional>
#include <memory>
#include <string>
#include <tuple>
#include <vector>

namespace pigcd {
namespace webapi {
struct request_data_t {
    std::string method;
    std::string url;
    std::vector<std::pair<std::string, std::string>> headers;
    std::vector<char> data;
};

using response_data_f = std::function<std::vector<char>(int)>;

struct response_data_t {
    std::string response_proto;
    int response_code;
    std::string response_comment;
    std::vector<std::pair<std::string, std::string>> response_headers;
    // response_data_f body_factory;
    std::shared_ptr<std::istream> body_factory;
};

inline response_data_f response_data_f_const_string(std::string s) {
    return [s](int n) -> std::vector<char> {
        if (n == 0) {
            return std::vector(s.begin(), s.end());
        } else
            return {};
    };
}
inline response_data_f response_data_f_const_vector(const std::vector<char> s) {
    return [s](int n) -> std::vector<char> {
        if (n == 0) {
            return s;
        } else
            return {};
    };
}

/**
 * @brief Get the header value object
 *
 * @tparam T target value type
 * @param e headers vector. Last header with the given key will be considered
 * @param key key to find. All lower case letters. For example "content-type"
 * @param default_value the default value. The type will be determined by this
 * value.
 * @return T value of the given header or default_value if none was found
 */
template <typename T>
T get_header_value(const std::vector<std::pair<std::string, std::string>> &e, std::string key, T default_value) {
    std::any ret = default_value;
    for (auto [k, v] : e) {
        if (k == key) {
            if (std::is_same_v<T, int>)
                ret = stoi(v);
            else if (std::is_same_v<T, double>)
                ret = stod(v);
            else if (std::is_same_v<T, unsigned>)
                ret = (unsigned)stoul(v);
            else if (std::is_same_v<T, unsigned long>)
                ret = stoul(v);
            else if (std::is_same_v<T, size_t>)
                ret = stoul(v);
            else if (std::is_same_v<T, char>)
                ret = v.size() ? v.at(0) : default_value;
            else
                ret = v;
        }
    }
    return std::any_cast<T>(ret);
}

/**
 * @brief starts the http server. It will loop forever.
 *
 * @param addr address to start port at, for example 0.0.0.0 fo any addr
 * @param port port number as a string
 * @param on_data_line callback function that will be called every time some
 * request comes. If callback throws throw std::runtime_error then it will stop
 * processing data and close sockets.
 */
void server(std::string addr, std::string port,
            std::function<pigcd::webapi::response_data_t(pigcd::webapi::request_data_t request, pigcd::webapi::response_data_t response)> on_request,
            std::function<void(int)> on_server_socket_handler_ready);

response_data_t send_request(std::string addr, std::string port, const request_data_t request);

void close_listen_socket(int server_socket);

}  // namespace webapi
}  // namespace pigcd
#endif
