/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#ifndef ___PIGCD_HPP____CONFIGURATION__
#define ___PIGCD_HPP____CONFIGURATION__

#include <sstream>

#include "motors.hpp"

namespace pigcd {
namespace config {

int from_args(int argc, char **argv);

int load(std::istream &cfg);
int save(std::ostream &f);

}  // namespace config
}  // namespace pigcd
#endif
