/**
 * @file main.cpp
 * @author Tadeusz Puźniakowski
 * @brief Allows for execution of simple gcode directly on raspberry pi
 * @version 0.1
 * @date 2022-01-31
 *
 * @copyright Copyright (c) 2022
 *
 */
/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <signal.h>

#include <chrono>
#include <distance_t.hpp>
#include <fstream>
#include <iostream>
#include <json.hpp>
#include <list>
#include <map>
#include <random>
#include <regex>
#include <sstream>
#include <thread>
#include <vector>

#include "buttons.hpp"
#include "configuration.hpp"
#include "gcd.hpp"
#include "gpio.hpp"
#include "motors.hpp"
#include "unit_conversion.hpp"
#include "vec3di.hpp"
#include "webapi.hpp"

using namespace tp::discrete;

namespace pigcd {

const std::string STATIC_FILES_DIRECTORY = "static";

namespace gpio {
#ifdef ARCH_x86_64
#warning "Using fake driver on x86 architecture. If you want it to work, compile it on the Raspberry Pi 2 or 3"
extern std::list<std::vector<unsigned int>> reported_fake_states;
void print_reported_fake_states();
#endif
}  // namespace gpio
}  // namespace pigcd

std::random_device rd;                              // Will be used to obtain a seed for the random number engine
std::mt19937 global_random_number_generator(rd());  // Standard mersenne_twister_engine seeded with rd()
std::string close_request_secret = std::to_string(std::uniform_int_distribution<>(0, 0x0fffffff)(global_random_number_generator));

////// API

pigcd::webapi::response_data_t request_handler_GET(pigcd::webapi::request_data_t request, const pigcd::webapi::response_data_t response_);
pigcd::webapi::response_data_t request_handler_bad_request(pigcd::webapi::request_data_t request, const pigcd::webapi::response_data_t response_);
pigcd::webapi::response_data_t request_handler_POST_exec(pigcd::webapi::request_data_t request, const pigcd::webapi::response_data_t response_);
pigcd::webapi::response_data_t request_handler_POST(pigcd::webapi::request_data_t request, const pigcd::webapi::response_data_t response_);
pigcd::webapi::response_data_t request_handler_GET_apidoc(pigcd::webapi::request_data_t request, const pigcd::webapi::response_data_t response);

////// END API

class gcode_executor_server_t {
    std::vector<std::string> _status_exec;
    std::vector<std::string> _lines;
    std::future<int> _execution;
    std::mutex _mutex;
    std::atomic<bool> _on_hold;
    std::atomic<int> _program_id;

    void push_back(const std::string s) {
        std::lock_guard<std::mutex> guard(_mutex);

        _status_exec.push_back(s);
    }
    void clear() {
        std::lock_guard<std::mutex> guard(_mutex);

        _status_exec.clear();
    }
    std::size_t exec_index() {
        std::lock_guard<std::mutex> guard(_mutex);
        return _status_exec.size();
    }

    static int gcode_executor(gcode_executor_server_t *pthis, const std::list<std::string> lines) {
        using namespace pigcd;
        using namespace std::chrono_literals;
        pthis->_lines = std::vector<std::string>(lines.begin(), lines.end());
        pthis->clear();
        for (auto line_p = lines.begin(); (line_p != lines.end()) && (pigcd::motors::is_thread_working); line_p++) {
            while ((pthis->_on_hold) && (!pthis->ignore_next_gcode)) {
                std::this_thread::sleep_for(100ms);
            }
            std::stringstream ret_stream;
            std::string line = *line_p;
            if (line.size() > 0) {
                if (line[0] == ';') {
                    ret_stream << "ok comment";
                } else if (!pthis->ignore_next_gcode) {
                    try {
                        ret_stream << pigcd::exec_gcd_line(line);
                    } catch (const std::exception &e) {
                        ret_stream << "ERROR EXECUTING '" << line << "': " << e.what();
                    }
                } else if (pthis->ignore_next_gcode) {
                    ret_stream << "ignorng command";
                }
            }
            pthis->push_back(ret_stream.str());
        }
        if (!pigcd::motors::is_thread_working) pthis->push_back("err: thread terminated");
        return lines.size();
    }

    nlohmann::json cycle_hold(std::list<std::string>) {
        _on_hold = true;
        return {{"status", "ok"}, {"note", "Cycle hold. Send single ! (exclamation mark) to resume."}};
    }
    nlohmann::json cycle_start(std::list<std::string>) {
        _on_hold = false;
        return {{"status", "ok"}, {"note", "Cycle start."}};
    }
    nlohmann::json current_status_compact(std::list<std::string>) {
        auto p_steps = pigcd::motors::get_global_machine_steps();
        auto p_mm = pigcd::unit_conversion::to_xyz(p_steps);
        auto buttons = pigcd::buttons::get_buttons_state();

        std::string buttons_txt;
        std::transform(buttons.begin(), buttons.end(), std::back_inserter(buttons_txt), [](auto e) { return (char)(e + '0'); });

        int idx = exec_index();

        return {{"status", std::string("<") + (is_gcode_executing() ? "Executing" : "Idle") + ",MPos:" + std::to_string(p_mm[0]) + "," +
                               std::to_string(p_mm[1]) + "," + std::to_string(p_mm[2]) + ",WPos:" + std::to_string(p_mm[0]) + "," + std::to_string(p_mm[1]) +
                               "," + std::to_string(p_mm[2]) + ",MSteps:" + std::to_string(p_steps[0]) + "," + std::to_string(p_steps[1]) + "," +
                               std::to_string(p_steps[2]) + ",Buttons:" + buttons_txt + ",Index:" + std::to_string(idx) + ">"},
                {"position", {{"x", p_mm[0]}, {"y", p_mm[1]}, {"z", p_mm[2]}}},
                {"steps", {{"x", p_steps[0]}, {"y", p_steps[1]}, {"z", p_steps[2]}}},
                {"buttons", buttons},
                {"exec_index", idx}};
    }

   public:
    bool is_gcode_executing() { return ((_execution.valid()) && (_execution.wait_for(std::chrono::seconds(0)) != std::future_status::ready)); }

    volatile std::atomic<bool> ignore_next_gcode = false;

    /**
     * @brief Get the status object that contains executed commands starting with first_idx
     *
     * @param first_idx first index to get
     * @return nlohmann::json
     */
    nlohmann::json get_status(unsigned first_idx = 0) {
        std::lock_guard<std::mutex> guard(_mutex);
        std::vector<nlohmann::json> ret;
        ret.reserve(_lines.size());
        for (auto i = first_idx; i < std::min(_status_exec.size() + 1, _lines.size()); i++) {
            nlohmann::json value = {{"command", _lines[i]}, {"i", i}};
            if (i < _status_exec.size()) value["log"] = _status_exec[i];
            ret.push_back(value);
        }
        return {{"log", ret}, {"id", (int)_program_id}, {"executing", is_gcode_executing()}};
    }

    nlohmann::json exec(const std::list<std::string> &lines) {
        if (lines.size() == 1) {
            if (*lines.begin() == "!") {  ///< cycle hold - pause execution of gcode
                return cycle_hold(lines);
            } else if (*lines.begin() == "~") {  ///< cycle start
                return cycle_start(lines);
            } else if (*lines.begin() == "?") {  ///< current status
                return current_status_compact(lines);
            }
        }
        std::lock_guard<std::mutex> guard(_mutex);

        if (_execution.valid()) {
            if (_execution.wait_for(std::chrono::seconds(0)) == std::future_status::ready) {
                // std::cerr << "# Previous G-CODE result " << _execution.get() << std::endl;
            } else {
                return {{"status", "error"}, {"error", "Only one G-CODE program can be run at once"}};
            }
        }
        _program_id = (_program_id + 1) & 0x07fffffff;
        _execution = std::async(std::launch::async, gcode_executor, this, lines);
        return {{"status", "ok"}, {"id", (int)_program_id}, {"note", "Running in parallel. You can check the status by "}};
    }
};
gcode_executor_server_t gcode_executor_server_obj;

void serve_file(std::string path, pigcd::webapi::response_data_t &response) {
    using namespace std;

    auto &response_headers = response.response_headers;
    if (path.size() > 4) {
        auto suffix = path.substr(path.find_last_of('.') + 1);
        if (ifstream(path).good()) {
            std::transform(suffix.begin(), suffix.end(), suffix.begin(), [](auto c) { return std::tolower(c); });
            if (suffix == "html")
                response_headers.push_back({"content-type", "text/html"});
            else if (suffix == "png")
                response_headers.push_back({"content-type", "image/png"});
            else if (suffix == "ico")
                response_headers.push_back({"content-type", "image/ico"});
            else if (suffix == "svg")
                response_headers.push_back({"content-type", "image/svg+xml"});
            response.body_factory = std::make_shared<std::ifstream>(path);  // pigcd::webapi::response_data_f_const_vector(str);
        } else {
            response.response_code = 404;
            response.response_comment = "not found";
            response_headers.push_back({"content-type", "text/html"});
            response.body_factory = std::make_shared<std::stringstream>("File not found.");  // pigcd::webapi::response_data_f_const_vector(str);
        }
    }
}

pigcd::webapi::response_data_t request_handler_GET_api_gcode_status(pigcd::webapi::request_data_t, const pigcd::webapi::response_data_t response_,
                                                                    const std::vector<std::string> url_array) {
    pigcd::webapi::response_data_t response = response_;
    unsigned start_idx = 0;
    if (url_array.size() > 3) try {
            start_idx = std::stoi(url_array[3]);
        } catch (std::exception &e) {
            start_idx = 0;
        }
    response.body_factory = std::make_shared<std::istringstream>(gcode_executor_server_obj.get_status(start_idx).dump());
    return response;
}

pigcd::webapi::response_data_t request_handler_GET(pigcd::webapi::request_data_t request, const pigcd::webapi::response_data_t response_) {
    using namespace std;
    pigcd::webapi::response_data_t response = response_;
    auto [method, url, headers, body] = request;
    auto &[response_protocol, response_status, response_status_comment, response_headers, response_body_gen_f] = response;

    auto body_string = string(body.begin(), body.end());

    stringstream url_stream(url);
    string element;
    vector<string> url_array;
    while (getline(url_stream, element, '/')) url_array.push_back(element);
    if (url_array.size() > 1) {
        if (url_array[1] == "api") {
            if (url_array[2] == "gcode_status") {
                return request_handler_GET_api_gcode_status(request, response, url_array);
            }
        } else {
            std::string path = pigcd::STATIC_FILES_DIRECTORY;
            for (auto pelem = url_array.begin() + 1; pelem < url_array.end(); pelem++) {
                if (*pelem != "..") path = path + "/" + *pelem;
            }
            // cout << "serving file (" << url << "): '" << path << "'" << endl;
            serve_file(path, response);
            return response;
        }
    }
    response_body_gen_f = std::make_shared<std::istringstream>(
        "<!DOCTYPE html><html lang='en'><head><meta "
        "charset='utf-8'><title>title</title></head><body><a "
        "href='index.html'>Go to testing page</a></body></html>");
    return response;
}

pigcd::webapi::response_data_t request_handler_bad_request(pigcd::webapi::request_data_t request, const pigcd::webapi::response_data_t response_) {
    auto [method, url, headers, body] = request;
    pigcd::webapi::response_data_t response = response_;
    auto &[response_protocol, response_status, response_status_comment, response_headers, response_body_gen_f] = response;

    auto body_string = std::string(body.begin(), body.end());

    response.response_code = 400;
    response_body_gen_f = std::make_shared<std::istringstream>(std::string("ERROR: ") + method + " URL:" + url);
    return response;
}

pigcd::webapi::response_data_t request_handler_POST_exec(pigcd::webapi::request_data_t request, const pigcd::webapi::response_data_t response_) {
    using namespace pigcd;
    pigcd::webapi::response_data_t response = response_;

    auto [method, url, headers, body] = request;
    auto &[response_protocol, response_status, response_status_comment, response_headers, response_body_gen_f] = response;

    auto body_string = std::string(body.begin(), body.end());

    std::stringstream input_lines(body_string);
    std::list<std::string> lines;
    std::string l;
    while (std::getline(input_lines, l)) lines.push_back(l);
    response_body_gen_f = std::make_shared<std::istringstream>(gcode_executor_server_obj.exec(lines).dump());
    return response;
}

pigcd::webapi::response_data_t request_handler_POST(pigcd::webapi::request_data_t request, const pigcd::webapi::response_data_t response_) {
    if (request.url == "/api/exec") {
        return request_handler_POST_exec(request, response_);
    } else {
        return request_handler_bad_request(request, response_);
    }
}

auto request_handlers_list = []() {
    std::list<std::tuple<std::string, std::string,
                         std::function<pigcd::webapi::response_data_t(pigcd::webapi::request_data_t request, const pigcd::webapi::response_data_t response)>>>
        ret_list;

    ret_list.push_back({"GET", "/apidoc", request_handler_GET_apidoc});
    ret_list.push_back({"GET", ".*", request_handler_GET});
    ret_list.push_back({"POST", ".*", request_handler_POST});
    ret_list.push_back({".*", ".*", request_handler_bad_request});
    return ret_list;
}();

pigcd::webapi::response_data_t request_handler_GET_apidoc(pigcd::webapi::request_data_t request, const pigcd::webapi::response_data_t response) {
    using namespace pigcd;
    pigcd::webapi::response_data_t response__ = response;

    auto body_string = std::string(request.data.begin(), request.data.end());

    std::string ret = "<ul>";

    for (auto &[method_m, url_m, handler] : request_handlers_list) {
        ret = ret + "<li>" + method_m + " " + url_m + "<li>\n";
    }
    ret = ret + "</ul>";
    response__.body_factory = std::make_shared<std::istringstream>(ret);
    return response__;
}

pigcd::webapi::response_data_t request_handler(pigcd::webapi::request_data_t request, const pigcd::webapi::response_data_t response) {
    auto [method, url, headers, body] = request;
    const auto [response_protocol, response_status, response_status_comment, response_headers, response_body_gen_f] = response;

    for (auto &[method_m, url_m, handler] : request_handlers_list) {
        std::regex method_r(method_m);
        std::regex url_r(url_m);
        std::smatch base_match;

        if (std::regex_match(method, base_match, method_r) && std::regex_match(url, base_match, url_r)) {
            return handler(request, response);
        }
    }
    return request_handler_bad_request(request, response);
}

void on_ctrl_c_signal(int) {
    pigcd::motors::is_thread_working = 0;  // terminate motors thread
}

void on_usr1_signal(int) { gcode_executor_server_obj.ignore_next_gcode = true; }

int main(int argc, char **argv) {
    using namespace pigcd;
    using namespace std::chrono_literals;

    std::cout << "close request secret: " << close_request_secret << std::endl;

    config::from_args(argc, argv);
    std::ifstream cfg("config.cfg");
    if (cfg.is_open()) {
        config::load(cfg);
    }
    config::from_args(argc, argv);
    config::save(std::cout);
    motors::init();
    gpio::init_in(buttons::io_buttons, std::vector<int>(buttons::io_buttons.size(), 1));

    auto steps_thread = motors::run_thread();
    std::string line;
    signal(SIGINT, on_ctrl_c_signal);
    signal(SIGUSR1, on_usr1_signal);

    int server_socket;
    auto async_server_fut =
        std::async(std::launch::async, webapi::server, "0.0.0.0", "12112", request_handler, [&server_socket](auto s) { server_socket = s; });

    using namespace std::chrono_literals;
    while (async_server_fut.wait_for(100ms) != std::future_status::ready) {
        if (pigcd::motors::is_thread_working == 0) {
            std::cout << "Closing PiGcd..." << std::endl;
            break;
        }
    }
    webapi::close_listen_socket(server_socket);
    pigcd::motors::is_thread_working = false;
    int ret = steps_thread.get();

    motors::disable_spindle(0, unit_conversion::seconds_to_ticks(1));
    return ret;
}
