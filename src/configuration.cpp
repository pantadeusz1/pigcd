/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "configuration.hpp"

#include <fstream>
#include <iostream>
#include <map>
#include <sstream>

#include "buttons.hpp"
#include "motors.hpp"
#include "unit_conversion.hpp"

using namespace std;

namespace pigcd {
namespace config {

std::map<std::string, std::function<void(std::string)>> options_setters;
std::map<std::string, std::function<std::string()>> options_getters;

bool register_setting(std::string name, const std::function<void(std::string)> &setter, const std::function<std::string()> &getter) {
    if (options_setters.count(name) != 0) throw std::invalid_argument("setting " + name + " already registered");
    options_setters[name] = setter;
    options_getters[name] = getter;
    return true;
}

#define REGISTER_APP_SETTING_ARRAY(paramname)                                                     \
    config::register_setting(                                                                     \
        #paramname,                                                                               \
        [](std::string value) -> void {                                                           \
            std::stringstream s(value);                                                           \
            unsigned i = 0;                                                                            \
            auto tmp = paramname.at(i);                                                           \
            decltype(tmp) a;                                                                      \
            while (s >> a) paramname.at(i++) = a;                                                 \
            if (i != paramname.size()) throw std::invalid_argument(std::string(#paramname) + " should have " + std::to_string(paramname.size()) + " elements"); \
        },                                                                                        \
        []() -> std::string {                                                                     \
            std::stringstream ret;                                                                \
            for (auto e : paramname) ret << " " << e;                                             \
            return ret.str().substr(1);                                                           \
        });

#define REGISTER_APP_SETTING_VALUE(paramname)                                                     \
    config::register_setting(                                                                     \
        #paramname,                                                                               \
        [](std::string value) -> void {                                                           \
            std::stringstream s(value);                                                           \
            s >> paramname;                                                                       \
        },                                                                                        \
        []() -> std::string {                                                                     \
            std::stringstream s;                                                                  \
            s << paramname;                                                                       \
            return s.str();                                                                       \
        });

std::string machine_name = "default";

bool arguments_ready = false;

void setup() {
    using namespace pigcd;
    using namespace std::chrono_literals;

    // xyz - pins - only at start!
    REGISTER_APP_SETTING_ARRAY(motors::io_steppers_dir);
    REGISTER_APP_SETTING_ARRAY(motors::io_steppers_en);
    REGISTER_APP_SETTING_ARRAY(motors::io_steppers_step);
    REGISTER_APP_SETTING_ARRAY(motors::io_spindles);

    REGISTER_APP_SETTING_ARRAY(buttons::io_buttons);

    // this can be changed in runtime
    REGISTER_APP_SETTING_ARRAY(unit_conversion::steps_per_millimeter);
    REGISTER_APP_SETTING_VALUE(motors::step_duration);

    REGISTER_APP_SETTING_VALUE(machine_name);

    arguments_ready = true;
}

int from_args(int argc, char **argv) {
    if (!arguments_ready) setup();
    int arguments_set = 0;
    if (argc > 1) {
        std::vector<std::string> arguments(argv + 1, argv + argc);
        for (auto arg = arguments.begin(); arg != arguments.end(); arg++) {
            auto conf_key = *arg;
            if (options_setters.count(conf_key)) {

                auto setter = options_setters[conf_key];
                arg++;
                if (arg != arguments.end()) {
                    setter(*arg);
                    arguments_set++;
                }
                else throw std::invalid_argument("you did not provide arguments for " + conf_key + ". Example value '" + options_getters[conf_key]() + "'");
            }
        }
    }
    return arguments_set;
}
int load(std::istream &cfg) {
    if (!arguments_ready) setup();
    using namespace pigcd;
    using namespace std::chrono_literals;

    string line;
    while (getline(cfg, line)) {
        stringstream ss(line);
        string name;
        ss >> name;
        if (options_setters.count(name)) {
            options_setters[name](line.substr(name.size() + 1));
        } else if ((!((name.size() > 0) && (name[0] == '#'))) && !(name.size() == 0)) {
            std::cerr << "Unrecognized config line: " << line << std::endl;
        }
    }
    return 0;
}

int save(std::ostream &f) {
    if (!arguments_ready) setup();
    using namespace pigcd;
    using namespace std::chrono_literals;
    for (auto [name, getter] : options_getters) {
        f << name << " " << getter() << std::endl;
    }
    return 0;
}

}  // namespace config
}  // namespace pigcd