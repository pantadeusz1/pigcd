/**
 * @file gpio-unknown.cpp
 * @author Tadeusz Puźniakowski
 * @brief GPIO implementation for Raspberry Pi 3
 * @version 0.1
 * @date 2022-01-31
 *
 * @copyright Copyright (c) 2022
 *
 */

/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#ifdef ARCH_armv7l

#include <chrono>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <thread>

#include "gpio.hpp"
// RASPBERRY PI GPIO - docs from http://www.pieter-jan.com/node/15

#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

// #define BCM2708_PERI_BASE       0x20000000   // raspi 1 //
#define BCM2708_PERI_BASE 0x3F000000  // raspi 3 //
#define GPIO_BASE (BCM2708_PERI_BASE + 0x200000)

#define BLOCK_SIZE (4 * 1024)

#define INP_GPIO(g) *(gpio.addr + ((g) / 10)) &= ~(7 << (((g) % 10) * 3))
#define OUT_GPIO(g) *(gpio.addr + ((g) / 10)) |= (1 << (((g) % 10) * 3))
#define SET_GPIO_ALT(g, a) *(gpio.addr + (((g) / 10))) |= (((a) <= 3 ? (a) + 4 : (a) == 4 ? 3 : 2) << (((g) % 10) * 3))

#define GPIO_SET *(gpio.addr + 7)   // sets   bits which are 1 ignores bits which are 0
#define GPIO_CLR *(gpio.addr + 10)  // clears bits which are 1 ignores bits which are 0

#define GPIO_READ(g) *(gpio.addr + 13) &= (1 << (g))

#define GET_GPIO(g) (*(gpio.addr + 13) & (1 << g))  // 0 if LOW, (1<<g) if HIGH

#define GPIO_PULL *(gpio.addr + 37)      // Pull up/pull down
#define GPIO_PULLCLK0 *(gpio.addr + 38)  // Pull up/pull down clock

namespace pigcd {
namespace gpio {

struct bcm2835_peripheral {
    unsigned long addr_p;
    int mem_fd;
    // void* map;
    volatile uint32_t* addr;
};

bcm2835_peripheral init_gpio_lib() {
    struct bcm2835_peripheral gpio;
    gpio = {GPIO_BASE, 0, 0};
    if ((gpio.mem_fd = open("/dev/mem", O_RDWR | O_SYNC)) < 0) {
        throw std::runtime_error("Failed to open /dev/mem, try checking permissions.\n");
    }
    auto map_ = mmap(NULL, BLOCK_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED,
                     gpio.mem_fd,  // File descriptor to physical memory virtual
                     // file '/dev/mem'
                     gpio.addr_p  // Address in physical map that we want this
                                  // memory block to expose
    );
    if (map_ == MAP_FAILED) {
        throw std::runtime_error("map_peripheral failed");
    }

    gpio.addr = (volatile uint32_t*)map_;

    using namespace std::chrono_literals;

    return gpio;
}

struct bcm2835_peripheral gpio = init_gpio_lib();

// 1 - output, 0 - input
// inspired by
// https://github.com/RPi-Distro/raspi-gpio/blob/master/raspi-gpio.c#L302 by Ja

void set_gpio_mode_x(int g, int fsel) {
    uint32_t reg = g / 10;
    uint32_t sel = g % 10;
    volatile uint32_t* tmp = gpio.addr + reg;
    *tmp = *tmp & (~(0x7 << (3 * sel)));  // with mask
    *tmp = *tmp | ((fsel & 0x7) << (3 * sel));
}

void init_in(const std::vector<int> pins, const std::vector<int> pullups) {
    using namespace std::chrono_literals;
    if (pins.size() != pullups.size())
        throw std::invalid_argument(
            "pins must be the same size as pullups - every pin have to be "
            "accompanied with information about pullup");
    unsigned int pull_value = 0;
    for (unsigned int i = 0; i < pins.size(); i++) {
        auto pin = pins[i];
        auto pullup = pullups[i];
        INP_GPIO(pin);
        // set_gpio_mode_x(pin,0);
        if (pullup) pull_value |= 1 << pin;
        std::cout << "setting up pin " << pins[i] << std::endl;
    }

    // enable pull-up on selected gpios (why? >...)
    GPIO_PULL = 2;
    std::this_thread::sleep_for(10us);
    GPIO_PULLCLK0 = pull_value;
    std::this_thread::sleep_for(10us);
    GPIO_PULL = 0;
    GPIO_PULLCLK0 = 0;
}
void init_out(const std::vector<int> pins) {
    for (auto pin : pins) {
        INP_GPIO(pin);
        OUT_GPIO(pin);
    }
}
void set_states(const std::vector<int>& pins, const std::vector<char>& states) {
    uint32_t set_clear_ignore_array[4] = {0, 0, 0, 0};
    for (unsigned int i = 0; i < pins.size(); i++) {
        set_clear_ignore_array[((int)states[i]) & 3] |= (1 << pins[i]);
    }
    GPIO_SET = set_clear_ignore_array[1];
    GPIO_CLR = set_clear_ignore_array[0];
}
char get_state(const int pin) { return (unsigned char)(1 - ((GPIO_READ(pin)) >> (pin))); }

}  // namespace gpio
}  // namespace pigcd

#endif
