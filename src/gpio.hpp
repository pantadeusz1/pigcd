/**
 * @file gpio.hpp
 * @author Tadeusz Puźniakowski
 * @brief Hardware GPIO module that allows for setting pin states
 * @version 0.1
 * @date 2022-01-31
 *
 * @copyright Copyright (c) 2022
 *
 */
/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#ifndef ___GPIO_HPP___
#define ___GPIO_HPP___

#include <vector>

namespace pigcd {
namespace gpio {
void init_in(const std::vector<int> pins, const std::vector<int> pullups);
void init_out(const std::vector<int> pins);
void set_states(const std::vector<int>& pins, const std::vector<char>& states);
char get_state(const int pin);
}  // namespace gpio
}  // namespace pigcd

#endif