/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#include "gcd.hpp"

#include <map>
#include <sstream>
#include <string>

namespace pigcd {

int goto_accelerate(
    const pigcd::unit_conversion::position_t& destination_p, double start_velocity, double acceleration_mms2, double minimal_velocity, double maximal_velocity,
    std::function<int(pigcd::unit_conversion::position_t& current_p, pigcd::unit_conversion::position_t& new_p, double current_v)> steps_executor);
void goto_xyz_variable_speed(unit_conversion::position_t new_position, double velocity_min, double velocity_max, double acceleration_max,
                             const motors::raw_exec_steps_flag_e flags);
std::string exec_gcd_command_g(std::map<char, std::string> args);
std::string exec_gcd_command_m(std::map<char, std::string> args);

int goto_accelerate(
    const pigcd::unit_conversion::position_t& destination_p, double start_velocity, double acceleration_mms2, double minimal_velocity, double maximal_velocity,
    std::function<int(pigcd::unit_conversion::position_t& current_p, pigcd::unit_conversion::position_t& new_p, double current_v)> steps_executor) {
    using namespace tp::discrete;
    int ret;
    pigcd::unit_conversion::position_t current_p = unit_conversion::to_xyz(motors::get_global_machine_steps());
    auto dt = unit_conversion::dt();
    auto current_v = start_velocity;
    auto vector_to_destination_1 = destination_p - current_p;
    double distance_max = vector_to_destination_1.length();
    if (distance_max < 0.01) return 0;
    vector_to_destination_1 = vector_to_destination_1 * (1.0 / distance_max);
    auto p0 = current_p;

    while ((p0 - current_p).length() < distance_max) {
        auto new_p = current_p + vector_to_destination_1 * current_v * dt + vector_to_destination_1 * current_v * acceleration_mms2 * 0.5 * dt * dt;
        current_v = std::max(std::min(current_v + acceleration_mms2 * dt, maximal_velocity), minimal_velocity);
        if ((ret = steps_executor(current_p, new_p, current_v)) != 0) return ret;
        current_p = new_p;
    }
    return ret;
};

void goto_xyz_variable_speed(unit_conversion::position_t new_position, double velocity_min, double velocity_max, double acceleration_max,
                             const motors::raw_exec_steps_flag_e stepping_flags) {
    if (acceleration_max < 5.0) throw std::invalid_argument("acceleration cannot be less than 5.0. The provided value is " + std::to_string(acceleration_max));
    using namespace tp::discrete;
    auto current_p = unit_conversion::to_xyz(motors::get_global_machine_steps());

    auto vector_to_destination = new_position - current_p;
    auto travel_distance = vector_to_destination.length();
    if (travel_distance == 0.0) {
        return;  // no move
    }
    auto vector_to_destination_norm = vector_to_destination / travel_distance;

    double break_distance = 0.0;
    try {
        goto_accelerate(current_p + vector_to_destination_norm * 10000, velocity_max, -acceleration_max, velocity_min, velocity_max,
                        [velocity_min](auto& current_p, auto&, double current_v) {
                            if (current_v <= velocity_min) throw current_p;
                            return 0;
                        });
    } catch (unit_conversion::position_t end_position_after_break) {
        break_distance = (end_position_after_break - current_p).length();
        if (break_distance > (travel_distance / 2)) break_distance = travel_distance / 2;
    }

    get_precision_time();  // we need this time NOW
    double current_velocity = velocity_min;
    auto steps_executor = [&current_velocity, stepping_flags](auto& current_p, auto& new_p, double current_v) {
        using namespace tp::discrete;

        current_velocity = current_v;
        pigcd::motors::machine_position_t dp_int = unit_conversion::to_steps(new_p) - unit_conversion::to_steps(current_p);
        return motors::raw_exec_steps(dp_int, stepping_flags);
    };
    if (goto_accelerate(new_position + (((vector_to_destination * (break_distance / travel_distance)) * (-1.0))), velocity_min, acceleration_max, velocity_min,
                        velocity_max, steps_executor) != 0)
        return;
    if (goto_accelerate(new_position, current_velocity, -acceleration_max, velocity_min, velocity_max, steps_executor) != 0) return;
}

std::string exec_gcd_command_g(std::map<char, std::string> args) {
    static double feedrate_g0 = 100;
    static double feedrate_g1 = 2;
    static bool absolute_coordinates = true;

    using namespace pigcd;
    using namespace std::chrono_literals;

    auto current_p = unit_conversion::to_xyz(motors::get_global_machine_steps());
    if (absolute_coordinates) {
        if (args.count('x')) current_p[0] = std::stof(args['x']);
        if (args.count('y')) current_p[1] = std::stof(args['y']);
        if (args.count('z')) current_p[2] = std::stof(args['z']);
    } else {
        if (args.count('x')) current_p[0] += std::stof(args['x']);
        if (args.count('y')) current_p[1] += std::stof(args['y']);
        if (args.count('z')) current_p[2] += std::stof(args['z']);
    }
    if (std::stoi(args['g']) == 0) {
        if (args.count('f')) feedrate_g0 = std::stof(args['f']);
        goto_xyz_variable_speed(current_p, std::min(5.0, feedrate_g0 / 2.0), feedrate_g0, 200.0, motors::raw_exec_steps_flag_e::NONE);
        return "ok ";
    } else if (std::stoi(args['g']) == 1) {
        if (args.count('f')) feedrate_g1 = std::stof(args['f']);
        goto_xyz_variable_speed(current_p, std::min(1.0, feedrate_g1 / 2.0), feedrate_g1, 200.0, motors::raw_exec_steps_flag_e::NONE);
        return "ok ";
    } else if (std::stoi(args['g']) == 28) {
        unit_conversion::position_t probe_distance;
        unit_conversion::position_t home_speeds = {50, 50, 4};
        unit_conversion::position_t home_distances = {-300, -300, 100};
        unit_conversion::position_t back_distances = {1, 1, 1};
        auto go_to_axis_probe = [&](int axis_idx) {
            motors::set_global_machine_steps({0, 0, 0});
            unit_conversion::position_t go_to = {0, 0, 0};
            go_to[axis_idx] = home_distances[axis_idx];
            goto_xyz_variable_speed(go_to, 0.5, home_speeds[axis_idx], 200, motors::raw_exec_steps_flag_e::PROBE_XYZ);
            std::this_thread::sleep_for(30ms);
            auto [probe_pos, probe_status] = motors::get_probe_status();
            std::this_thread::sleep_for(30ms);
            motors::reset_probe_status();
            go_to[axis_idx] = -back_distances[axis_idx] * (go_to[axis_idx] / std::abs(go_to[axis_idx]));
            probe_distance[axis_idx] = unit_conversion::to_xyz(probe_pos)[axis_idx] + go_to[axis_idx];
            motors::set_global_machine_steps({0, 0, 0});
            goto_xyz_variable_speed(go_to, 0.5, home_speeds[axis_idx], 200.0, motors::raw_exec_steps_flag_e::NONE);
        };
        go_to_axis_probe(2);
        go_to_axis_probe(0);
        go_to_axis_probe(1);
        std::stringstream ret_stream;
        ret_stream << "ok " << probe_distance[0] << " " << probe_distance[1] << " " << probe_distance[2] << std::endl;
        motors::set_global_machine_steps(unit_conversion::to_steps(probe_distance));
        return ret_stream.str();
    } else if (std::stoi(args['g']) == 90) {  // Switch to absolute coordinates
        absolute_coordinates = true;
        return "ok absolute";
    } else if (std::stoi(args['g']) == 91) {  // Switch to relative coordinates
        absolute_coordinates = false;
        return "ok relative";
    } else if (std::stoi(args['g']) == 92) {  // Set current machine position
        if (args.count('x')) current_p[0] = std::stof(args['x']);
        if (args.count('y')) current_p[1] = std::stof(args['y']);
        if (args.count('z')) current_p[2] = std::stof(args['z']);
        motors::set_global_machine_steps(unit_conversion::to_steps(current_p));
        return "ok";
    }
    return "error:20\n";
}

std::string exec_gcd_command_m(std::map<char, std::string> args) {
    using namespace pigcd;
    using namespace std::chrono_literals;

    switch (std::stoi(args['m'])) {
        case 3:
            motors::enable_spindle(0, unit_conversion::seconds_to_ticks(5));
            return "ok";
        case 5:
            motors::disable_spindle(0, unit_conversion::seconds_to_ticks(0.1));
            return "ok";
        default:
            return std::string("err unsupported M") + args['m'] + " code";
    }
}

std::string get_current_settings_string() { return "TODO: Show settings\n$0=...\n$1=..."; }
std::string exec_gcd_command_$(std::map<char, std::string>) { return "ok"; }
std::string exec_gcd_line(std::string line) {
    using namespace pigcd;
    using namespace std::chrono_literals;
    std::stringstream ret_stream;
    std::transform(line.begin(), line.end(), line.begin(), [](unsigned char c) { return std::tolower(c); });
    std::map<char, std::string> args;
    unsigned i = 0;
    char c = ' ';
    while (i < line.size()) {
        if (((line[i] >= 'a') && (line[i] <= 'z')) || (line[i] == '$') || (line[i] == '=')) {
            if ((c == '$') && (line[i] == '$')) return get_current_settings_string();
            c = line[i];
        } else if (((line[i] >= '0') && (line[i] <= '9')) || (line[i] == '-') || (line[i] == '.')) {
            args[c] = args[c] + line[i];
        }
        i++;
    }
    if (args.count('m')) {
        ret_stream << exec_gcd_command_m(args);
    } else if (args.count('g')) {
        ret_stream << exec_gcd_command_g(args);
    } else if (args.count('$')) {
        ret_stream << exec_gcd_command_$(args);
    }
    return ret_stream.str();
}

}  // namespace pigcd
