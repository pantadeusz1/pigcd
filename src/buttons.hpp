/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#ifndef __BUTTONS_IN_PIGCD__HPP___
#define __BUTTONS_IN_PIGCD__HPP___

#include <algorithm>
#include <array>
#include <chrono>
#include <future>
#include <map>
#include <stdexcept>
#include <thread>
#include <vector>

#include "vec3di.hpp"
namespace pigcd {

namespace buttons {
extern std::vector<int> io_buttons;
std::vector<int> get_buttons_state();
}  // namespace buttons

}  // namespace pigcd

#endif
