/**
 * @file vec3di.hpp
 * @author Tadeusz Puźniakowski
 * @brief Simple discrete vector math
 * @version 0.1
 * @date 2022-01-31
 *
 * @copyright Copyright (c) 2022
 *
 */
/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#ifndef ____VEC3DI__HPP____
#define ____VEC3DI__HPP____

#include <array>

namespace tp {
namespace discrete {
using vec3di_t = std::array<int, 3>;

inline vec3di_t operator+(const vec3di_t a, const vec3di_t b) { return {a[0] + b[0], a[1] + b[1], a[2] + b[2]}; }
inline vec3di_t operator-(const vec3di_t a, const vec3di_t b) { return {a[0] - b[0], a[1] - b[1], a[2] - b[2]}; }
inline vec3di_t operator-(const vec3di_t a, const int b) { return {a[0] * b, a[1] * b, a[2] * b}; }
inline vec3di_t operator-(const int b, const vec3di_t a) { return {a[0] * b, a[1] * b, a[2] * b}; }

}  // namespace discrete
}  // namespace tp

#endif