/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "webapi.hpp"

#include <netdb.h>
#include <unistd.h>

#include <any>
#include <chrono>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <thread>

namespace pigcd {
namespace webapi {

/**
 * @brief Start listening server on the host and port
 *
 * @param host host name to start server
 * @param port port number or service name
 * @return std::tuple<int,std::string, std::string> the values of
 * {listening_socket, actual_host, actual_port}
 */
std::tuple<int, std::string, std::string> do_listen(std::string host, std::string port) {
    int yes = 1;
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int s;
    int sdd;

    for (unsigned int i = 0; i < sizeof(struct addrinfo); i++) ((char *)&hints)[i] = 0;
    hints.ai_family = AF_UNSPEC;      // Allow IPv4 or IPv6
    hints.ai_socktype = SOCK_STREAM;  // Datagram socket
    hints.ai_flags = AI_PASSIVE;      // For wildcard IP address
    hints.ai_protocol = 0;            // Any protocol
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    s = getaddrinfo(host.c_str(), port.c_str(), &hints, &result);
    if (s != 0) {
        std::stringstream ss;
        ss << "ListeningSocket getaddrinfo:: " << gai_strerror(s) << "; port= " << port;
        throw std::invalid_argument(ss.str());
    }

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sdd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sdd == -1) continue;

        if (setsockopt(sdd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
            throw std::invalid_argument(
                "error at: setsockopt( "
                "sfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof( int ) )");
        }

        if (bind(sdd, rp->ai_addr, rp->ai_addrlen) == 0) {
            break;
        }
        ::close(sdd);
    }

    if (rp == NULL) {
        std::stringstream ss;
        ss << "ListeningSocket unable to bind address:: " << gai_strerror(s) << "; " << port;
        throw std::invalid_argument(ss.str());
    }
    freeaddrinfo(result);
    if (listen(sdd, 32) == -1) {
        throw std::invalid_argument("listen error");
    }

    return {sdd, host, port};
}

/**
 * @brief accepts connection on listening socket
 *
 * @param sfd listening socket file descriptor
 * @return std::tuple<int,std::string, std::string> connected socket, host_name,
 * port_name
 */
std::tuple<int, std::string, std::string> do_accept(int sfd) {
    struct sockaddr_storage peer_addr;
    socklen_t peer_addr_len = sizeof(struct sockaddr_storage);

    std::string host_s;
    std::string service_s;
    int csfd;

    if ((csfd = ::accept(sfd, (struct sockaddr *)&peer_addr, &peer_addr_len)) == -1) {
        throw std::invalid_argument("could not accept connection!");
    } else {
        char host[NI_MAXHOST], service[NI_MAXSERV];
        getnameinfo((struct sockaddr *)&peer_addr, peer_addr_len, host, NI_MAXHOST, service, NI_MAXSERV, NI_NUMERICSERV);
        host_s = host;
        service_s = service;
    }
    return {csfd, host_s, service_s};
}

/**
 * @brief read the request line. In case of some unexpected situation it will
 * report to standard output
 *
 * @param s
 * @return std::string
 */
std::string read_line(int s) {
    char c[2] = {0, 0};
    std::string ret = "";
    auto bytesRead = ::read(s, c, 1);
    while (bytesRead > 0) {
        ret = ret + c[0];
        if ((c[0] == '\n') && (c[1] == '\r')) {
            break;
        }
        c[1] = c[0];
        bytesRead = ::read(s, c, 1);
        if (bytesRead < 0) {
            std::cout << "bytesRead = " << bytesRead << "; ERRNO " << hstrerror(errno) << std::endl;
            throw std::invalid_argument("Could not correctly read line from HTTP client");
        }
    }
    if (ret.length() >= 2) return ret.substr(0, ret.length() - 2);
    return ret;
}

std::tuple<std::string, std::string, std::string> method_line(std::string line) {
    auto delim = line.find(':');
    std::vector<std::string> parts;
    while ((delim = line.find(' ')) != std::string::npos) {
        parts.push_back(line.substr(0, delim));
        line = line.substr(delim + 1);
    }
    parts.push_back(line);
    if (parts.size() == 3) {
        return {parts[0], parts[1], parts[2]};
    } else
        return {"", "", ""};
}

std::pair<std::string, std::string> response_status_line(std::string line) {
    std::stringstream ss(line);
    std::string version, status_code;
    ss >> version >> status_code;
    return {version, status_code};
}

std::pair<std::string, std::string> header_line(std::string line) {
    auto delim = line.find(':');
    if (delim == std::string::npos) {
        return {"", ""};
    }
    std::transform(line.begin(), line.begin() + delim, line.begin(), [](unsigned char c) { return std::tolower(c); });
    return {line.substr(0, delim), line.substr(delim + 1)};
}

std::vector<std::string> get_header_values(const std::vector<std::pair<std::string, std::string>> &e, std::string key) {
    std::vector<std::string> ret;
    for (auto [k, v] : e)
        if (k == key) ret.push_back(v);
    return ret;
}

std::string generate_response_begin(int code = 200, std::string code_comment = "ok") {
    std::stringstream sent;
    sent << "HTTP/1.1 " << code << " - " << code_comment;
    sent << "\r\nServer: pigcd " << __DATE__;
    sent << "\r\nConnection: close";
    sent << "\r\n\r\n";

    return sent.str();
}

int send_response(int client_socket, response_data_t &response) {
    size_t written_bytes = 0;
    std::stringstream sent;
    auto &[response_proto, response_code, response_comment, response_headers, response_data_f] = response;
    sent << response_proto << " " << response_code << " - " << response_comment;
    for (auto [k, v] : response_headers) {
        sent << "\r\n" << k << ": " << v;
    }
    sent << "\r\n\r\n";
    auto sent_str = sent.str();
    written_bytes += write(client_socket, sent_str.data(), sent_str.size());

    std::vector<char> buffer;
    buffer.reserve(65536);
    for (auto rbyte = response_data_f->get(); rbyte != EOF; rbyte = response_data_f->get()) {
        buffer.push_back((char)rbyte);
        if (buffer.size() > 65530) {
            written_bytes += write(client_socket, buffer.data(), buffer.size());
            buffer.clear();
            buffer.reserve(65536);
        }
    }
    if (buffer.size() > 0) written_bytes += write(client_socket, buffer.data(), buffer.size());
    shutdown(client_socket, 1);
    close(client_socket);

    return written_bytes;
}

std::pair<std::vector<std::pair<std::string, std::string>>, std::vector<char>> read_headers_and_body(int client_socket) {
    std::string line;
    std::vector<std::pair<std::string, std::string>> headers;
    while ((line = read_line(client_socket)).size() > 0) {
        headers.push_back(header_line(line));
        auto [key, value] = header_line(line);
    }
    size_t data_size = get_header_value(headers, "content-length", 0);
    std::vector<char> data(data_size);
    if (data_size > 0) {
        read(client_socket, data.data(), data_size);
    }
    shutdown(client_socket, 0);
    return {headers, data};
}
void close_listen_socket(int server_socket) { shutdown(server_socket, SHUT_RD); }
void server(std::string addr, std::string port,
            std::function<pigcd::webapi::response_data_t(pigcd::webapi::request_data_t request, pigcd::webapi::response_data_t response)> on_request,
            std::function<void(int)> on_server_socket_handler_ready) {
    std::cout << "Listening on: " << addr << " : " << port << std::endl;
    auto [srv_socket, srv_addr, srv_port] = do_listen(addr, port);
    on_server_socket_handler_ready(srv_socket);
    bool finish = false;
    do {
        auto [client_socket, client_addr, client_port] = do_accept(srv_socket);
        try {
            std::string line = read_line(client_socket);
            auto [method, url, version] = method_line(line);
            auto [headers, data] = read_headers_and_body(client_socket);
            pigcd::webapi::response_data_t response = {"HTTP/1.1",
                                                       200,  // status code
                                                       "ok",
                                                       {{"connection", "close"}, {"Server", std::string("pigcd ") + __DATE__}},  // headers
                                                       std::make_shared<std::stringstream>("")};
            try {
                response = on_request({method, url, headers, data}, response);
                send_response(client_socket, response);
            } catch (const std::runtime_error &e) {
                response.response_code = 300;
                response.response_comment = "closing";
                response.body_factory = std::make_shared<std::stringstream>("closing server");
                send_response(client_socket, response);
                finish = true;
            } catch (const std::exception &e) {
                response.response_code = 500;
                response.response_comment = "internal server error";
                response.body_factory = std::make_shared<std::stringstream>(std::string("something bad happened: ") + e.what());
                send_response(client_socket, response);
            }
        } catch (const std::invalid_argument &e) {
            std::cerr << "Bad request: " << e.what() << std::endl;
        }
    } while (!finish);
    close(srv_socket);
}

response_data_t send_request(std::string addr, std::string port, const request_data_t request) {
    auto [method, url, headers, body] = request;
    // std::vector<std::pair<std::string, std::string>> headers;
    headers.push_back({"host", addr + ":" + port});
    headers.push_back({"connection", "close"});

    struct addrinfo hints;
    // bzero((char *)&hints, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;      ///< IPv4 or IPv6
    hints.ai_socktype = SOCK_STREAM;  ///< stream socket

    struct addrinfo *addr_p;
    int err = getaddrinfo(addr.c_str(), port.c_str(), &hints, &addr_p);
    if (err) {
        throw std::runtime_error(gai_strerror(err));
    }
    struct addrinfo *rp;
    // find first working address that we can connect to
    for (rp = addr_p; rp != NULL; rp = rp->ai_next) {
        int connected_socket = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (connected_socket != -1) {
            if (connect(connected_socket, rp->ai_addr, rp->ai_addrlen) != -1) {
                std::stringstream request_stream;
                request_stream << method << " " << url << " "
                               << "HTTP/1.1"
                               << "\r\n";
                if (body.size())
                    request_stream << "content-length"
                                   << ": " << body.size() << "\r\n";
                for (auto [k, v] : headers) {
                    if (k != "content-length") request_stream << k << ": " << v << "\r\n";
                }
                request_stream << "\r\n";
                auto headers_and_requ = request_stream.str();
                write(connected_socket, headers_and_requ.data(), headers_and_requ.size());
                if (body.size()) write(connected_socket, body.data(), body.size());
                shutdown(connected_socket, 1);
                std::string line = read_line(connected_socket);

                auto [version, status_code] = response_status_line(line);
                auto [headers, data] = read_headers_and_body(connected_socket);

                close(connected_socket);

                freeaddrinfo(addr_p);  // remember - cleanup
                return {version, std::stoi(status_code), "---", headers, std::make_shared<std::stringstream>(std::string(data.begin(), data.end()))};
            }
        }
    }
    freeaddrinfo(addr_p);
    throw std::invalid_argument(std::string("could not open connection to ") + addr + ":" + port);
}

}  // namespace webapi
}  // namespace pigcd