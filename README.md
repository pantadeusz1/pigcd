# Native Raspberry Drive

By Tadeusz Puźniakowski

Allow running grbl compatible g-code on raspberry pi withoud grbl-shield. Just native GPIO state manipulation.

## Getting started

Compile it:

```bash
make
```

Run it:

```bash
./build/pigcd
```

## Interaction via HTTP

```bash
curl ....
```

### Default settings

#### Stepstick configuration:

| role                  | X   | Y   | Z     |
|----------------------:|-----|-----|-------|
| dir                   | 27  | 4   | 9     |
| en                    | 10  | 10  | 10    |
| step                  | 22  | 17  | 11    |
| steps_per_millimeter  | 100 | 100 | -1600 |

Spindle pin: 18



## Roadmap

TODO

## Contributing

TOD

## Authors and acknowledgment

Tadeusz Puźniakowski

## License

GNU GPL
